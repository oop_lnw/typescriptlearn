function printStatusCode(code: string | number| boolean) {
    console.log(`My status code is ` + code)
  }
  printStatusCode(404);
  printStatusCode('4044');
  printStatusCode(false);