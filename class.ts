class Person {
    private name: string;
  
    public constructor(name: string) {
      this.name = name;
    }
  
    public getName(): string {
      return this.name;
    }
  }
  




  const person = new Person("Jane");
  const person2 = new Person("NekoSama");

  console.log(person.getName()); // person.name isn't accessible from outside the class since it's private
  console.log(person2.getName()); // person.name isn't accessible from outside the class since it's private