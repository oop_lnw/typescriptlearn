let u:any = true;
u = "string"; // Error: Type 'string' is not assignable to type 'boolean'.
console.log(Math.round(u)); // Error: Argument of type 'boolean' is not assignable to parameter of type 'number'.